# secret-detection

```
For each iteration/example, place words here to look for, and update the secret-detection-ruleset.toml. If you set the search to an empty string, it should resolve everything

20220519 secret
20220609 secret
20220518 secret
20220508 secret
20220619 secret
```

## GraphQL

### Query Vulnerabilities

The following JSON query is "sufficient" GraphQL for now:

```json
{
	"query" : "{project(fullPath: \"glen_miller/secret-detection\") { vulnerabilities(sort: detected_desc) { nodes{ confirmedAt detectedAt dismissedAt resolvedAt reportType resolvedOnDefaultBranch falsePositive state mergeRequest { id }}}}}"
}
```

response

```json
{
	"data": {
		"project": {
			"vulnerabilities": {
				"nodes": [
					{
						"confirmedAt": null,
						"detectedAt": "2022-05-19T22:44:42Z",
						"dismissedAt": null,
						"resolvedAt": null,
						"reportType": "SECRET_DETECTION",
						"resolvedOnDefaultBranch": false,
						"falsePositive": false,
						"state": "DETECTED",
						"mergeRequest": null
					},
					{
						"confirmedAt": null,
						"detectedAt": "2022-05-09T15:18:11Z",
						"dismissedAt": "2022-06-03T18:54:09Z",
						"resolvedAt": null,
						"reportType": "SECRET_DETECTION",
						"resolvedOnDefaultBranch": true,
						"falsePositive": false,
						"state": "DISMISSED",
						"mergeRequest": null
					},
					{
						"confirmedAt": null,
						"detectedAt": "2022-05-09T15:14:52Z",
						"dismissedAt": null,
						"resolvedAt": null,
						"reportType": "SECRET_DETECTION",
						"resolvedOnDefaultBranch": false,
						"falsePositive": false,
						"state": "DETECTED",
						"mergeRequest": null
					},
					{
						"confirmedAt": null,
						"detectedAt": "2022-05-09T15:14:52Z",
						"dismissedAt": null,
						"resolvedAt": null,
						"reportType": "SECRET_DETECTION",
						"resolvedOnDefaultBranch": true,
						"falsePositive": false,
						"state": "DETECTED",
						"mergeRequest": null
					},
					{
						"confirmedAt": null,
						"detectedAt": "2022-05-09T15:03:56Z",
						"dismissedAt": null,
						"resolvedAt": null,
						"reportType": "SECRET_DETECTION",
						"resolvedOnDefaultBranch": true,
						"falsePositive": false,
						"state": "DETECTED",
						"mergeRequest": null
					},
					{
						"confirmedAt": null,
						"detectedAt": "2022-05-09T14:56:43Z",
						"dismissedAt": null,
						"resolvedAt": null,
						"reportType": "SECRET_DETECTION",
						"resolvedOnDefaultBranch": true,
						"falsePositive": false,
						"state": "DETECTED",
						"mergeRequest": null
					}
				]
			}
		}
	}
}
```

change the `fullPath` as appropriate and set a token in your headers (standard API token for the instance)

#### Notes

* Items that have the `Vulnerabiliey remediated, please verify` message are items that have `resolvedOnDefaultBranch: true`
* `Needs Triage` is `state: DETECTED`

### Change (mutate) Vulnerabilities

You can mitigate items off of the default branch report using a mutation similar to below:

```graphql
mutation {
  vulnerabilityDismiss(
    input: {clientMutationId: "gillers-vuln-dismisser", comment: "This is the comment on the dismiss", id: "gid://gitlab/Vulnerability/44980289", dismissalReason:NOT_APPLICABLE}
  ) {
		vulnerability {
			state
			notes {
				nodes {
					body
					createdAt
					author {
						id
					}
				}
			}
		}
		clientMutationId
		errors
  	}
}
```

#### Notes

The `comment` is added to the `vulnerabilty finding` on the merge request pipeline that discovered the vulnerability

![image.png](./image.png)

![image-1.png](./image-1.png)

### Errata

* The job to update the `Security Dashboard` runs at 1:15 UTC. Need to research which job does this, and if that schedule is adjustable.
    * ![image-2.png](./image-2.png)
* https://gitlab.com/gitlab-org/gitlab/-/tree/master/ee/app/services/vulnerabilities
