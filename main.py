import json
from ariadne import gql
import json
import requests
from logging import getLogger, StreamHandler, FileHandler, Formatter, DEBUG
from datetime import datetime
import os

logger = None

DRY_RUN = True

project_path = None
host = None
token = None

vuln_query = gql("""
query {
  project(fullPath: "PROJECT_PATH") {
    vulnerabilities(first: 2, after: "END_CURSOR", sort: detected_desc) {
      pageInfo {
        endCursor
        hasNextPage
      }
      nodes {
        mergeRequest{
          id
        }
        detectedAt
        resolvedOnDefaultBranch
        id
        reportType
        title
        severity
        scanner {
          externalId
          name
          vendor
        }
        identifiers {
          externalType
          externalId
          name
          url
        }
        falsePositive
        project {
          id
          name
          fullPath
        }
        description
        links {
          name
          url
        }    
        state
      }
    }
  }
}

""")

clientMutationId = "gillers-vuln-dismisser"
dismissComment = "My dismissal comment"

vuln_mutation = gql("""
mutation {
  vulnerabilityDismiss(
    input: {clientMutationId: "CLIENT_MUTATION_ID", comment: "DISMISS_COMMENT", id: "VULNERABILITY_ID", dismissalReason:NOT_APPLICABLE}
  ) {
    vulnerability {
      id
      state
      discussions(last: 1) {
        nodes {
          id
        }
      }
    }    
    errors
  }
}
""")

note_mutation = gql("""
mutation {
  createNote(
    input: {noteableId: "VULNERABILITY_ID", discussionId: "DISCUSSION_ID", body: "DISCUSSION_NOTE"}
  ) {
    errors
  }
}
""")

header = {
  "AUTHORIZATION": f"Bearer {token}",
  "Content-Type": "application/json"
}

def setup_logging():
  global logger 
  logger = getLogger("vulns")
  logger.setLevel(DEBUG)
  log_file_format = \
      "[%(asctime)s][%(levelname)s]|%(module)s.%(funcName)s:%(lineno)d| %(message)s"
  stderr_log_handler = StreamHandler()
  formatter = Formatter(log_file_format, datefmt="%d %b %Y %H:%M:%S")
  log_path = f"vulnrun-{datetime.now().timestamp()}.log"
  file_log_handler = FileHandler(log_path)
  file_log_handler.setFormatter(formatter)
  logger.addHandler(file_log_handler)
  stderr_log_handler.setFormatter(formatter)
  logger.addHandler(stderr_log_handler)

def post_graphql_data_and_return_json(data, host, token):
  if not data:
    logger.error(f"No data sent to post")
    return None

  header = {
    "AUTHORIZATION": f"Bearer {token}",
    "Content-Type": "application/json"
  }

  try:
    resp = requests.post(url=host, headers=header, data=json.dumps(data))

    if resp and resp.status_code == 200 and resp.json():
      if resp.json():
        return resp.json()
      else:
        logger.warning(f"200 status with no JSON")
        return None
  except requests.RequestException as re:
    logger.error(f"Error posting to GraphQL endpoint:\n{re}")
    return None

def dig(dictionary, *args, default=None):
    """
        Recursive dictionary key lookup function

        Example:
            dig({"nest": {"hello": {"world": "this is nested"}}}, "nest", "hello")
            >>>> {'world': 'this is nested'}

        :param dictionary: (dict) dictionary to traverse
        :param *args: (tuple) series of keys to dig through
        :return: If the most nested key is found, the value of the key

    """
    if not args:
        return dictionary
    if isinstance(dictionary, dict):
        for i, arg in enumerate(args):
            found = dictionary.get(arg, None)
            if found is not None:
                if isinstance(found, dict):
                    args = args[i + 1:]
                    return dig(found, *args, default=default)
                return found
            return default
    return default

def main():

  setup_logging()

  if not (project_path := os.getenv("GITLAB_PROJECT_PATH")):
  #  "glen_miller/secret-detection"
    logger.error(f"Please set GITLAB_PROJECT_PATH")
    exit(1)

  if not (host := os.getenv("GITLAB_HOST")): 
    logger.error(f"Please set GITLAB_HOST")
    exit(1)

  if not (token := os.getenv("GITLAB_TOKEN")):
    logger.error(f"Please set GITLAB_TOKEN")
    exit(1)

  if not (DRY_RUN := os.getenv("DRY_RUN")):
    DRY_RUN = True
  else:
    DRY_RUN = False

  # The first query has no end cursor
  end_cursor = None
  flat = vuln_query.replace("PROJECT_PATH", project_path).replace("\n","").replace("\"END_CURSOR\"", f"\"{end_cursor}\"" if end_cursor else "null")
  data = {"query":f"{flat}"}
  
  project_vuln_list = {}
  detected_project_vuln_ids = []
  vuln_nodes = []
  
  while True:
    # Query the vulnerabilities for a project
    project_vuln_list = post_graphql_data_and_return_json(data, host, token)
    
    # TODO: ERROR CHECK
    if not project_vuln_list or project_vuln_list == {}:
      logger.error(f"Unable to get vulnerability list or list empty for project {project_path}. Exiting")
      exit(1)

    # Get vulnerability nodes
    if not (vuln_nodes := dig(project_vuln_list, "data", "project", "vulnerabilities", "nodes")):
      logger.warning(f"Found project, but no vulnerabilities returned")
      exit(1)

    # Grab all of the vulns with a state of DETECTED for remediation
    for n in vuln_nodes:
      if n.get("state") == "DETECTED":    
        detected_project_vuln_ids.append(n.get("id"))

    if not detected_project_vuln_ids or len(detected_project_vuln_ids) == 0:
      logger.info(f"No vulnerabilities for project {project_path} detected")
      exit(0)

    for d in detected_project_vuln_ids:
      data = {"query": vuln_mutation.replace("CLIENT_MUTATION_ID", clientMutationId).replace("DISMISS_COMMENT",dismissComment).replace("VULNERABILITY_ID", d).replace("\n","")}
      logger.info(f"{'DRY_RUN: 'if DRY_RUN else ''}Attempting to mutate {d} for project {project_path}")
      if not DRY_RUN:
        mutation_response = post_graphql_data_and_return_json(data, host, token)
        errors = dig(mutation_response, "data", "vulnerabilityDismiss", "errors")
        if not mutation_response or (errors and len(errors) > 0):
          logger.error(f"Problem setting status for vulnerability {d} for project {project_path}\nErrors: {'None response from query' if not mutation_response else errors}\nMoving to the next.")        
        else:
          logger.info(f"Mutation response is:\n{json.dumps(mutation_response)}")
          # Data mutated. Get the discussion ID
          discussion_id = mutation_response.get("data").get("vulnerabilityDismiss").get("vulnerability").get("discussions").get("nodes")[0].get("id")
          data = {"query": note_mutation.replace("VULNERABILITY_ID", d).replace("DISCUSSION_ID", discussion_id).replace("DISCUSSION_NOTE", f"Auto-remediated by script on {datetime.now()}")}
          logger.info(f"{'DRY_RUN: 'if DRY_RUN else ''}Making note for {d} for project {project_path}")
          discussion_response = post_graphql_data_and_return_json(data, host, token)
          errors = dig(discussion_response, "data", "createnote", "errors")
          if not discussion_response or (errors and len(errors) > 0):
            logger.error(f"{'DRY_RUN: 'if DRY_RUN else ''}Problem setting note for vulnerability {d} for project {project_path}\nErrors: {'None response from query' if not discussion_response else errors}")
      else:
        logger.info(f"{'DRY_RUN: 'if DRY_RUN else ''}No work performed")

    # Pagination info
    if dig(project_vuln_list, "data", "project", "vulnerabilities", "pageInfo", "hasNextPage"):
      end_cursor = dig(project_vuln_list, "data", "project", "vulnerabilities", "pageInfo", "endCursor")
      logger.info(f"{'DRY_RUN: 'if DRY_RUN else ''}Fetch next page after cursor: {end_cursor}")
    else:
      break

    flat = vuln_query.replace("PROJECT_PATH", project_path).replace("\n","").replace("\"END_CURSOR\"", f"\"{end_cursor}\"" if end_cursor else "null")
    data = {"query":f"{flat}"}
    detected_project_vuln_ids = []

if __name__ == "__main__":
  main()